package com.shustoff.mvp.render

class StateRenderer<State>(
    private val renderToView: (State) -> Unit
) {

    private var state: State? = null
    private var rendered: Boolean = false

    fun render(state: State) {
        if (this.state != state || !rendered) {
            this.state = state
            rendered = true
            renderToView.invoke(state)
        }
    }
}

fun <State> renderer(renderToView: (State) -> Unit) = StateRenderer(renderToView)