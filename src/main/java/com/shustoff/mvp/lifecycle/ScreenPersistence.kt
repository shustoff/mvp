package com.shustoff.mvp.lifecycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner

internal class ScreenPersistence<T : Any>(
    val item: T
) : ViewModel() {

    class Factory<TItem : Any>(
        val creator: () -> TItem
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ScreenPersistence(creator.invoke()) as T
        }
    }
}

inline fun <reified T: Any> ViewModelStoreOwner.persistent(
    key: String? = null,
    noinline creator: () -> T
): T {
    return persistent(T::class.java, key, creator)
}

fun <T : Any> ViewModelStoreOwner.persistent(
    clazz: Class<out T>,
    key: String? = null,
    creator: () -> T
): T {
    val modelKey = "${clazz.canonicalName}:${key.orEmpty()}"
    return ViewModelProvider(this,
        ScreenPersistence.Factory(creator)
    )
        .get(modelKey, ScreenPersistence::class.java).item as T
}
