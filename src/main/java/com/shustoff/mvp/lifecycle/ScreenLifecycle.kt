package com.shustoff.mvp.lifecycle

import androidx.lifecycle.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first

interface ScreenLifecycle {
    suspend fun runOnEveryActivation(block: suspend () -> Unit)
    suspend fun runOnceActive(block: suspend () -> Unit)
    fun addOnCloseListener(listener: () -> Unit)
}

//TODO:ss make sure it works without calling the method after each onCreate
fun <Host> Host.getOrCreateScreenLifecycle(): ScreenLifecycle
        where Host : ViewModelStoreOwner, Host : LifecycleOwner {
    return ViewModelProvider(this).get(ScreenLifecycleImpl::class.java).also {
        it.bindLifecycle(this)
    }
}

internal class ScreenLifecycleImpl : ViewModel(), ScreenLifecycle {

    private val isActiveChannel = MutableStateFlow(false)
    private val onCloseListeners = mutableListOf<() -> Unit>()

    private var boundToLifecycle = false
    private val lifecycleObserver = object : LifecycleEventObserver {
        override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
            when (event) {
                Lifecycle.Event.ON_RESUME -> onActive()
                Lifecycle.Event.ON_PAUSE -> onInactive()
                Lifecycle.Event.ON_DESTROY -> {
                    source.lifecycle.removeObserver(this)
                    boundToLifecycle = false
                }
            }
        }
    }

    fun bindLifecycle(lifecycleOwner: LifecycleOwner) {
        if (boundToLifecycle) return

        boundToLifecycle = true
        lifecycleOwner.lifecycle.addObserver(lifecycleObserver)
        isActiveChannel.value = lifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED
    }

    fun onInactive() {
        isActiveChannel.value = false
    }

    fun onActive() {
        isActiveChannel.value = true
    }

    override fun onCleared() {
        super.onCleared()
        onCloseListeners.onEach { it.invoke() }
    }

    override suspend fun runOnEveryActivation(block: suspend () -> Unit) {
        isActiveChannel.collectLatest { isActive ->
            if (isActive) {
                block.invoke()
            }
        }
    }

    override suspend fun runOnceActive(block: suspend () -> Unit) {
        isActiveChannel.filter { it }.first()
        block.invoke()
    }

    override fun addOnCloseListener(listener: () -> Unit) {
        onCloseListeners.add(listener)
    }
}

