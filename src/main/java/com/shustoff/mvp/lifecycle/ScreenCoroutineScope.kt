package com.shustoff.mvp.lifecycle

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

interface ScreenCoroutineScope : CoroutineScope

fun ViewModelStoreOwner.screenCoroutineScope(): ScreenCoroutineScope =
    ViewModelProvider(this).get(ScreenCoroutineScopeImpl::class.java)

internal class ScreenCoroutineScopeImpl : ViewModel(),
    ScreenCoroutineScope {
    internal val job = Job()
    override val coroutineContext: CoroutineContext = Dispatchers.Default + job

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}
