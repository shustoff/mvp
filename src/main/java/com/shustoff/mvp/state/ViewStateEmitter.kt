package com.shustoff.mvp.state

import kotlinx.coroutines.flow.Flow

interface ViewStateEmitter<State : Any> {
    val state: Flow<State>
}
