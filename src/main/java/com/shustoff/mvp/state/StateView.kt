package com.shustoff.mvp.state

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

abstract class StateView<State : Any>(
    private val stateEmitter: ViewStateEmitter<State>,
    lifecycle: LifecycleOwner
) {
    protected var state: State? = null
        private set

    init {
        val isActive = MutableStateFlow(lifecycle.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
        val lifecycleObserver = object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                when (event) {
                    Lifecycle.Event.ON_RESUME -> isActive.value = true
                    Lifecycle.Event.ON_PAUSE -> isActive.value = false
                    Lifecycle.Event.ON_DESTROY -> lifecycle.lifecycle.removeObserver(this)
                }
            }

        }
        lifecycle.lifecycle.addObserver(lifecycleObserver)
        lifecycle.lifecycleScope.launch {
            stateEmitter.state
                .combine(isActive) { x, y -> x to y }
                .filter { (_, active) -> active }
                .map { (state, _) -> state }
                .collect { state ->
                    this@StateView.state = state
                    onRenderState(state)
                }
        }
    }

    protected abstract fun onRenderState(state: State)
}
