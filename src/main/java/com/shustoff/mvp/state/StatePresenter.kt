package com.shustoff.mvp.state

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterNotNull

abstract class StatePresenter<State : Any>(
    initialState: State? = null
) : ViewStateEmitter<State> {

    private val internalChannel: MutableStateFlow<State?> = MutableStateFlow(initialState)
    override val state: Flow<State> = internalChannel.filterNotNull()

    protected val lastState: State? get() = internalChannel.value

    init {
        if (initialState != null) {
            renderState(initialState)
        }
    }

    protected fun renderState(state: State) {
        internalChannel.value = state
    }
}
