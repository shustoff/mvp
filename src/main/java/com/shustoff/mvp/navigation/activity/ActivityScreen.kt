package com.shustoff.mvp.navigation.activity

import com.shustoff.mvp.navigation.ScreenBase
import java.io.Serializable

interface ActivityScreen : Serializable,
    ScreenBase<ActivityRoute>