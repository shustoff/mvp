package com.shustoff.mvp.navigation.activity

import android.content.Intent
import com.shustoff.mvp.navigation.ScreenBase
import com.shustoff.mvp.navigation.ScreenComponent
import java.io.Serializable

interface ScreenActivity<T> :
    ScreenComponent<T> where T : ScreenBase<ActivityRoute>, T : Serializable {

    fun getIntent(): Intent?

    fun setIntent(intent: Intent)

    override val screen: T
        get() {
            val intent = getIntent() ?: Intent().also { setIntent(it) }
            val screen = intent.getSerializableExtra(SCREEN) as? T
            return screen ?: buildScreen().also { screen ->
                intent.putExtra(SCREEN, screen)
            }
        }

    fun buildScreen(): T {
        throw Exception("Activity was started a wrong way without screen params")
    }

    companion object {
        private const val SCREEN = "activity_screen_key"

        fun <T> fillIntent(intent: Intent, screen: T) where T : ScreenBase<ActivityRoute>, T : Serializable {
            intent.putExtra(SCREEN, screen)
        }
    }
}