package com.shustoff.mvp.navigation.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.shustoff.mvp.navigation.ScreenBase
import com.shustoff.mvp.navigation.ScreenWithResult
import java.io.Serializable
import kotlin.reflect.KClass

interface ActivityRoute {
    fun navigate(
        from: Activity,
        options: Bundle?
    )
}

private class ActivityRouteImpl<TActivity, T>(
    private val activityClass: KClass<TActivity>,
    private val screen: T
) : ActivityRoute
    where TActivity : ScreenActivity<T>,
          TActivity : Activity,
          T : ScreenBase<ActivityRoute>,
          T : Serializable {

    override fun navigate(
        from: Activity,
        options: Bundle?
    ) {
        val intent = Intent(from, activityClass.java)
        ScreenActivity.fillIntent(
            intent,
            screen
        )

        if (this is ScreenWithResult<*>) {
            from.startActivityForResult(intent, 0, options)
        } else {
            from.startActivity(intent, options)
        }
    }
}

fun <T, TActivity> T.activityRoute(
    clazz: KClass<TActivity>
): ActivityRoute
    where TActivity : Activity,
          TActivity : ScreenActivity<T>,
          T : ScreenBase<ActivityRoute>,
          T : Serializable {
    return ActivityRouteImpl(clazz, this)
}