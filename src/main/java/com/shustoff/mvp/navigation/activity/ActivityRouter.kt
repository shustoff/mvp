package com.shustoff.mvp.navigation.activity

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.github.ajalt.timberkt.Timber
import com.shustoff.mvp.navigation.ActiveScreensRegistry
import com.shustoff.mvp.navigation.RouterBase
import com.shustoff.mvp.navigation.ScreenBase
import com.shustoff.mvp.navigation.ScreenWithResult
import java.lang.ref.WeakReference

abstract class ActivityRouter(
    app: Application
) : RouterBase<ActivityRoute>() {

    private var currentActivity: WeakReference<Activity>? = null

    init {
        app.registerActivityLifecycleCallbacks(LifecycleCallback())
    }

    override fun navigateBack(from: ScreenBase<ActivityRoute>) {
        Timber.d { "navigateBack ${currentActivity?.get()?.javaClass}" }
        currentActivity?.get()
            ?.takeUnless { it.isFinishing }
            ?.takeIf { it is ScreenActivity<*> && it.screen.instanceId == from.instanceId }
            ?.finish()
    }

    override fun navigateTo(to: ScreenBase<ActivityRoute>) {
        //TODO:ss animations
        Timber.d { "navigateTo ${currentActivity?.get()?.javaClass}" }
        currentActivity?.get()?.let {
            val route = to.createRoute()
            route.navigate(it, null)
        }
    }

    inner class LifecycleCallback : Application.ActivityLifecycleCallbacks {

        override fun onActivityStarted(activity: Activity) {
            Timber.d { "onActivityStarted ${activity.javaClass}" }
        }

        override fun onActivityStopped(activity: Activity) {
            Timber.d { "onActivityStopped ${activity.javaClass}" }
        }

        override fun onActivityDestroyed(activity: Activity) {
            Timber.d { "onActivityDestroyed ${activity.javaClass}" }
            if (activity.isFinishing && activity is ScreenActivity<*>) {
                try {
                    val screen = activity.screen
                    if (screen is ScreenWithResult<*>) {
                        ActiveScreensRegistry.postNoResult(screen.instanceId)
                    }
                } catch (e: Exception) {
                    Timber.wtf(e)
                }
            }
        }

        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        }

        override fun onActivityPaused(activity: Activity) {
            Timber.d { "onActivityPaused ${activity.javaClass}" }
            if (currentActivity?.get() === activity) {
                currentActivity = null
            }
        }

        override fun onActivityResumed(activity: Activity) {
            Timber.d { "onActivityResumed ${activity.javaClass}" }
            currentActivity = WeakReference(activity)
        }

        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            Timber.d { "onActivityCreated ${activity.javaClass}" }
        }

    }
}
