package com.shustoff.mvp.navigation

import com.github.ajalt.timberkt.Timber

abstract class RouterBase<TRoute> {

    suspend fun <TResult, ResultScreen> navigateToAndWaitForResult(
        from: ScreenBase<*>,
        to: ResultScreen
    ): TResult? where ResultScreen : ScreenWithResult<TResult>, ResultScreen : ScreenBase<TRoute> {
        navigateTo(to)

        Timber.d { "navigateToAndWaitForResult $from -> $to" }
        return ActiveScreensRegistry.waitForResult(from, to)
    }

    fun <TScreen, TResult> postResultAndClose(
        screen: TScreen,
        result: TResult?
    ) where TScreen : ScreenBase<TRoute>, TScreen : ScreenWithResult<TResult> {
        Timber.d { "postResultAndClose $screen: $result" }
        ActiveScreensRegistry.postResult(screen, result)
        navigateBack(screen)
    }

    abstract fun navigateBack(from: ScreenBase<TRoute>)

    abstract fun navigateTo(
        to: ScreenBase<TRoute>
    )

    protected fun onScreenStateChanged(screenId: String, isActive: Boolean) {
        Timber.d { "onScreenStateChanged $screenId: isActive = $isActive" }
        ActiveScreensRegistry.onScreenStateChanged(screenId, isActive)
    }
}
