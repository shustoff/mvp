package com.shustoff.mvp.navigation

interface ScreenComponent<T: ScreenBase<*>> {
    val screen: T
}