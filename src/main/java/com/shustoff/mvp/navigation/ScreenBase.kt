package com.shustoff.mvp.navigation

interface ScreenBase<TRoute> {
    val instanceId: String

    fun createRoute(): TRoute
}