package com.shustoff.mvp.navigation

import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first

internal object ActiveScreensRegistry {

    private val resultsChannel = BroadcastChannel<Pair<String, Any?>>(Channel.CONFLATED)
    private val activeScreenIds = mutableSetOf<String>()
    private val onActiveScreenChanged = BroadcastChannel<Unit>(Channel.CONFLATED)

    private var lastPostedScreenId: String? = null

    suspend fun <TResult, TResultScreen> waitForResult(
        from: ScreenBase<*>,
        to: TResultScreen
    ): TResult? where TResultScreen : ScreenWithResult<TResult>, TResultScreen: ScreenBase<*> {
        val (_, result) = resultsChannel.asFlow()
            .filter { (key, _) -> key == to.instanceId }
            .combine(
                onActiveScreenChanged.asFlow()
                    .filter { from.instanceId in activeScreenIds }) { it, _ -> it }
            .first()
        return result as? TResult
    }

    fun onScreenStateChanged(screenId: String, active: Boolean) {
        if (active) {
            activeScreenIds.add(screenId)
        } else {
            activeScreenIds.remove(screenId)
        }
        onActiveScreenChanged.offer(Unit)
    }

    fun <TScreen, TResult> postResult(
        screen: TScreen,
        result: TResult?
    ) where TScreen : ScreenBase<*>, TScreen : ScreenWithResult<TResult> {
        lastPostedScreenId = screen.instanceId
        resultsChannel.offer(screen.instanceId to result)
    }

    fun postNoResult(
        screenInstanceId: String
    ) {
        if (lastPostedScreenId != screenInstanceId) {
            lastPostedScreenId = screenInstanceId
            resultsChannel.offer(screenInstanceId to null)
        }
    }
}