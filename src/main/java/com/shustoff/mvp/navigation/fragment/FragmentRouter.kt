package com.shustoff.mvp.navigation.fragment

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.*
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import com.github.ajalt.timberkt.Timber
import com.shustoff.mvp.navigation.RouterBase
import com.shustoff.mvp.navigation.ScreenBase
import com.shustoff.mvp.navigation.ScreenWithResult
import java.lang.ref.WeakReference

abstract class FragmentRouter(
    private val containerId: Int
) : RouterBase<FragmentRoute>() {

    private var fragmentManager: WeakReference<FragmentManager>? = null
    private var activity: WeakReference<Activity>? = null

    private val fragmentCallback = FragmentLifecycle()

    fun bindToActivity(activity: FragmentActivity) {
        val observer = object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                if (event == Lifecycle.Event.ON_DESTROY) {
                    activity.lifecycle.removeObserver(this)

                    fragmentManager = null
                    activity.supportFragmentManager.unregisterFragmentLifecycleCallbacks(
                        fragmentCallback
                    )
                }
            }
        }
        activity.lifecycle.addObserver(observer)
        fragmentManager = WeakReference(activity.supportFragmentManager)
        this.activity = WeakReference(activity)
        activity.supportFragmentManager.registerFragmentLifecycleCallbacks(fragmentCallback, true)

        if (fragmentManager?.get()?.findFragmentById(containerId) == null) {
            navigateTo(getStartScreen())
        }
    }

    override fun navigateBack(from: ScreenBase<FragmentRoute>) {
        activity?.get()?.runOnUiThread {
            navigateBackInternal(from)
        }
    }

    private fun navigateBackInternal(from: ScreenBase<FragmentRoute>) {
        fragmentManager?.get()?.apply {
            val dialogFragment = findFragmentByTag(dialogTag) as? DialogFragment
            if (dialogFragment != null) {
                dialogFragment.takeIf { it is ScreenFragment<*> && it.screen.instanceId == from.instanceId }
                    ?.dismiss()
            }
            if ((findFragmentById(containerId) as? ScreenFragment<*>)?.screen?.instanceId == from.instanceId) {
                popBackStack()
            }
        }
    }

    override fun navigateTo(
        to: ScreenBase<FragmentRoute>
    ) {
        //TODO:ss animations
        Timber.d { "navigateTo $to" }
        activity?.get()?.runOnUiThread {
            navigateToInternal(to)
        }
    }

    private fun navigateToInternal(to: ScreenBase<FragmentRoute>) {
        val route = to.createRoute()

        try {
            if (route.isRoot) {
                fragmentManager?.get()
                    ?.popBackStack(rootStackTag, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }

            val fragment = route.createFragment()
                .also { it.arguments?.putString(fragmentScreenIdKey, to.instanceId) }
            (fragmentManager?.get()?.findFragmentByTag(dialogTag) as? DialogFragment)?.dismiss()
            if (fragment is DialogFragment) {
                fragmentManager?.get()?.let { fragment.show(it, dialogTag) }
            } else {
                fragmentManager?.get()
                    ?.beginTransaction()
                    ?.replace(containerId, fragment)
                    ?.also {
                        if (kotlin.runCatching {
                                fragmentManager?.get()?.findFragmentById(containerId)
                            }.getOrNull() != null && !route.isRoot
                        ) {
                            it.addToBackStack(rootStackTag)
                        }
                    }
                    ?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    ?.commitAllowingStateLoss()
            }
        } catch (e: Exception) {
            Timber.wtf(e)
        }
    }

    abstract fun getStartScreen(): FragmentScreen

    private inner class FragmentLifecycle : FragmentManager.FragmentLifecycleCallbacks() {
        override fun onFragmentCreated(
            fm: FragmentManager,
            f: Fragment,
            savedInstanceState: Bundle?
        ) {
            if (f is ScreenFragment<*>) {
                val screen = f.screen
                if (screen is ScreenWithResult<*>) {
                    val viewModel = ViewModelProvider(f).get(PostNullOnCleanupViewModel::class.java)
                    viewModel.screenInstanceId = screen.instanceId
                }
            }
        }

        override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
            val screen = (f as? ScreenFragment<*>)?.screen
            val screenId = screen?.instanceId
                ?: f.arguments?.getString(fragmentScreenIdKey)
                ?: return

            onScreenStateChanged(screenId, isActive = true)
        }

        override fun onFragmentPaused(fm: FragmentManager, f: Fragment) {
            val screen = (f as? ScreenFragment<*>)?.screen
            val screenId = screen?.instanceId
                ?: f.arguments?.getString(fragmentScreenIdKey)
                ?: return

            onScreenStateChanged(screenId, isActive = false)
        }
    }

    companion object {
        private const val fragmentScreenIdKey = "fragment_screen_id_key"
        private const val rootStackTag = "root_tag"
        private const val dialogTag = "dialog_tag"
    }
}
