package com.shustoff.mvp.navigation.fragment

import androidx.fragment.app.Fragment

interface FragmentRoute {

    val isRoot: Boolean

    fun createFragment(): Fragment
}