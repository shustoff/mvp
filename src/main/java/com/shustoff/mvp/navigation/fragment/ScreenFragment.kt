package com.shustoff.mvp.navigation.fragment

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.shustoff.mvp.navigation.ScreenBase
import com.shustoff.mvp.navigation.ScreenComponent
import java.io.Serializable

interface ScreenFragment<T> :
    ScreenComponent<T> where T : ScreenBase<FragmentRoute>, T : Serializable {

    fun getArguments(): Bundle?

    fun setArguments(args: Bundle?)

    fun getActivity(): FragmentActivity?

    override val screen: T get() = getArguments()!!.getSerializable(SCREEN) as T

    fun <T> setScreen(
        screen: T
    ) where T : Serializable {
        val args = getArguments() ?: Bundle()
        args.putSerializable(SCREEN, screen)
        setArguments(args)
    }

    companion object {
        private const val SCREEN = "fragment_screen_key"
    }
}