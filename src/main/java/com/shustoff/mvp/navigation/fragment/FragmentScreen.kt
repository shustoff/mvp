package com.shustoff.mvp.navigation.fragment

import com.shustoff.mvp.navigation.ScreenBase
import java.io.Serializable

interface FragmentScreen : Serializable,
    ScreenBase<FragmentRoute>