package com.shustoff.mvp.navigation.fragment

import androidx.lifecycle.ViewModel
import com.github.ajalt.timberkt.Timber
import com.shustoff.mvp.navigation.ActiveScreensRegistry

class PostNullOnCleanupViewModel : ViewModel() {
    var screenInstanceId: String? = null

    init {
        Timber.d { "created" }
    }

    override fun onCleared() {
        super.onCleared()
        Timber.d { "onCleared" }
        screenInstanceId?.also { ActiveScreensRegistry.postNoResult(it) }
    }
}