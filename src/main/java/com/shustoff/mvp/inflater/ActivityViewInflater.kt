package com.shustoff.mvp.inflater

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout

class ActivityViewInflater(
        private val activity: Activity
) : RootViewInflater {

    override fun inflateResource(layoutRes: Int): View {
        val rootView = FrameLayout(activity)
        activity.setContentView(rootView)
        return activity.layoutInflater.inflate(layoutRes, rootView, true)
    }

    override fun <T> inflateBinding(binding: (LayoutInflater, ViewGroup, Boolean) -> T): T {
        val rootView = FrameLayout(activity)
        activity.setContentView(rootView)
        return binding.invoke(activity.layoutInflater, rootView, true)
    }
}