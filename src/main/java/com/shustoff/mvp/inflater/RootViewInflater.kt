package com.shustoff.mvp.inflater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

interface RootViewInflater {

    fun inflateResource(@LayoutRes layoutRes: Int) : View

    fun <T> inflateBinding(bindingInflateMethod: (LayoutInflater, ViewGroup, Boolean) -> T): T

}