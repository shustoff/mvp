package com.shustoff.mvp.inflater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout

class FragmentViewInflater(
        private val inflater: LayoutInflater
) : RootViewInflater {

    val rootView = FrameLayout(inflater.context)

    override fun inflateResource(layoutRes: Int): View {
        rootView.removeAllViews()
        return inflater.inflate(layoutRes, rootView, true)
    }

    override fun <T> inflateBinding(binding: (LayoutInflater, ViewGroup, Boolean) -> T): T {
        rootView.removeAllViews()
        return binding.invoke(inflater, rootView, true)
    }
}